""" Manipulated from: https://github.com/carpedm20/deep-rl-tensorflow """

import numpy as np

class History:
  """ Defines a data-structure that gets the most recent frames to input to a
  Q-network. """

  def __init__(self, data_format, history_length, screen_dims):
    """ Initialises the history data-structure.

    Parameters
      data_format: Format to return sequences in. Valid values
                   are ['NHWC', 'NCHW'].
      history_length: Number of frames in a sequence.
      screen_dims: Dimensions of each frame in a sequence [height, width].
    """
    self.data_format = data_format
    self.history = np.zeros([history_length] + screen_dims, dtype=np.float32)

  def add(self, screen):
    """ Adds the frame to history.

    Parameters
      screen: Frame to add to history.
    """
    self.history[:-1] = self.history[1:]
    self.history[-1] = screen

  def get(self):
    """ Returns a sequence of the most recent frames.

    Returns
      Sequence of frames.
    """
    if self.data_format == 'NHWC' and len(self.history.shape) == 3:
      return np.transpose(self.history, (1, 2, 0))
    else:
      return self.history