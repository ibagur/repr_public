from gan import GAN
import tensorflow as tf
from logger import Logger
from experience import Experience
import sys, os
import numpy as np

# command line arguments
flags = tf.app.flags
flags.DEFINE_string('exp_load_dir', 'exp_load_folder/', 'Directory to load the experience replay from.')
flags.DEFINE_string('gan_load_dir', 'gan_load_folder/', "Directory to load the pseudo-items from. DQN also loaded from here if 'use_d2' == True.")
flags.DEFINE_string('save_dir', 'gan_save_folder/', 'Directory to make saves in.')
flags.DEFINE_boolean('restore', False, 'Whether the training session should be restored (True) or not (False).')
flags.DEFINE_string('mode', 'train', "Mode to run the GAN in ['train'].")
flags.DEFINE_boolean('pr', False, "Whether training should be done with sequences from the experience replay (True) or both the experience replay and pseudo-items (False).")
flags.DEFINE_float('ratio', 0.5, "Ratio between new task sequences and previously generated sequences (only applied when 'pr' == True).")
flags.DEFINE_boolean('use_d2', False, "Whether to use a second discriminator to improve the GAN for pseudo-rehearsal (True) or not (False).")
flags.DEFINE_float('scale_d2', 1000., "Scaling parameter between the two discriminators (only used when 'use_d2' == True).")
flags.DEFINE_string('teach_gen_filename', None, "Directory to load a replay buffer of generated items from (incl. file extension). If this is not None, this buffer of generated items is used to teach the new task to LTM.")
flags.DEFINE_integer('seed', 0, "Seed used for training/testing.")
args = flags.FLAGS
np.random.seed(args.seed)
tf.set_random_seed(args.seed)

def main(_):
  """ Runs the training function for the GAN """

  # TODO: have hyper-parameters here specified by command line arguments
  data_format = 'NCHW'
  experience_length = int(2e5)
  history_length = 4
  observation_dims = [84, 84]
  in_dims = [history_length]+observation_dims
  t_max = 200*500
  batch_size = 100

  if args.restore:
    assert(os.path.isdir(args.save_dir)), "Error[main]: could not find args.save_dir (%s) to restore!" % args.save_dir
  else:
    assert(not os.path.isdir(args.save_dir)), "Error[main]: args.save_dir (%s) already exists!" % args.save_dir
    os.makedirs(args.save_dir)

  log = Logger(args.save_dir, "gan")

  gan = GAN(tf.Session(), batch_size, in_dims, log, args.save_dir, False, args.use_d2, args.scale_d2)

  if args.teach_gen_filename is None:
    experience = Experience(data_format, int(batch_size*args.ratio) if args.pr else batch_size, history_length, experience_length, observation_dims)
    experience.restore(args.exp_load_dir)
    dataset = experience
  else:
    from gen_replay import GenReplay
    dataset = GenReplay(args.teach_gen_filename, int(batch_size*args.ratio) if args.pr else batch_size)

  if args.mode == 'train':
    gan.train(dataset, t_max, args.pr, args.ratio, args.restore, args.gan_load_dir)



# run on GPU0
if __name__ == '__main__':
  os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
  os.environ["CUDA_VISIBLE_DEVICES"] = "0"

  tf.app.run()