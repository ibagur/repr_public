from dqn_cf import DQN_CF
import numpy as np
import tensorflow as tf
import sys, os
import gzip
import pickle

class DQN_EWC(DQN_CF):
  """ Defines a DQN agent which can use a dual memory system. This agent retains
  previously learnt knowledge through (online-)EWC. """
  def __init__(self, sess, env, env_test, prev_env_test, args):
    """ Initialises the DQN agent.

    Parameters
      sess: Tensorflow session.
      env: Current environment to learn/play (should be a child class of Env).
      env_test: Current environment to be tested in (should be a child class of Env).
      prev_env_test: List of previously learnt environments to be tested in (should be a child
                     class of Env).
      args: Command line arguments defining the DQN agent.
    """
    assert(len(prev_env_test) <= 2), "'DQN_EWC' does not support task sequences longer than 3."
    self.ewc_lambda = args.ewc_lambda
    self.ewc_n_samples = args.ewc_n_samples
    if len(prev_env_test) == 2:
      self.prev_load_dir = args.prev_load_dir[0]
    else:
      self.prev_load_dir = None
    self.online_ewc = args.online_ewc
    self.ewc_discount = args.ewc_discount
    assert(not args.dual_head), "'dual_head' cannot be True when using EWC."
    DQN_CF.__init__(self, sess, env, env_test, prev_env_test, args)

  def get_update_record_names(self):
    """ Names of statistics to be recorded by Stats.

    Returns
      List of names.
    """
    return ['av_q', 'av_loss', 'av_dqn_loss', 'av_ewc_loss']

  def find_best_net_from_loss(self):
    """ Returns that Stats should record the best network based upon either:
    - Dual memory system: the minimum average loss received over the current
                          recording period.
    - Standard DQN: the most recent network.

    Returns
      Boolean.
    """
    if self.wm:
      return True
    return None

  def build_networks(self):
    """ Builds the networks used by the DQN agent. """
    DQN_CF.build_networks(self)
    self.s_copy = tf.placeholder(tf.float32, [None,] + self.in_dims,
      name='s_copy')
    self.s_prev_net = tf.placeholder(tf.float32, [None,] + self.in_dims,
      name='s_prev_net')
    self.pred_network_copy = self.network(self.s_copy, "pred_network", True, reuse=True)
    self.prev_network = self.network(self.s_prev_net, "prev_network", False)

  def build_loss(self):
    """ Builds the loss function used by the DQN agent. """
    with tf.variable_scope('loss'):
      DQN_CF.build_loss(self)
      self.dqn_loss = self.loss

      self.F_prev_W_placeholders = []
      self.Fs_placeholders = []
      self.ewc_loss = 0
      i = 0
      for _ in range(1 if self.prev_load_dir is None or self.online_ewc else 2):
        for w in sum(self.pred_network_copy.weights,[]):
          self.F_prev_W_placeholders.append(tf.placeholder(tf.float32, w.get_shape(), name='fisher_prev_W_placeholder_'+str(i)))
          self.Fs_placeholders.append(tf.placeholder(tf.float32, w.get_shape(), name='fisher_placeholder_'+str(i)))
          self.ewc_loss += tf.reduce_sum(self.Fs_placeholders[-1]*tf.square(self.F_prev_W_placeholders[-1]-w))
          i += 1
      # ewc_loss is divided by batch_size so that the ewc_lambda values are
      # effective in the same range of values used by the published EWC implementation.
      # We believe this is because they would have used the equivalent to
      # tf.reduce_sum for their standard DQN loss function (which the EWC constraint
      # is added to), whereas we are using tf.reduce_mean (which the EWC constraint
      # is added to), and thus we divide the EWC constraint by batch_size to make
      # their scales equal.
      self.ewc_loss = self.ewc_lambda/2 * self.ewc_loss/self.batch_size
      self.loss = self.dqn_loss + self.ewc_loss

  def calc_fisher(sess, model, model_input, prev_experience, ewc_n_samples, n_actions):
    """ Calculates the Fisher information matrix for the model.

    Parameters
      sess: Tensorflow Session.
      model: Model with weights to calculate matrix for.
      model_input: Input tensor for the model.
      prev_experience: Experience replay for the task the matrix is being calculated
                       for.
      ewc_n_samples: Number of samples from the experience replay to use to calculate
                     the matrix.
      n_actions: Number of output actions in the model.

    Returns
      List of Fisher information matrices, one for each weight tensor in the model.
    """
    Fs = []
    for i in range(len(sum(model.weights, []))):

      get_dervs = []
      for n in range(ewc_n_samples):
        F = []

        exp_ims = prev_experience.sample()[0]

        for j in range(n_actions):

          if n == 0:
            get_dervs.append(tf.gradients(model.outputs[:,j], sum(model.weights, [])[i], stop_gradients=sum(model.weights, [])))
          dervs = sess.run(get_dervs[j], feed_dict={model_input: exp_ims[:1]})
          dervs = dervs[0]
          F.append(dervs)

        F = np.array(F)
        w_shape = F.shape[1:]
        F = F.reshape((F.shape[0], -1))

        # The following block is used instead of:
        #   F = np.diag(F.T @ F).reshape(w_shape)
        # because otherwise it uses too much memory.
        F_diag = []
        for k in range(len(F.T)):
          F_diag.append(np.sum(F.T[k] * F[:,k]))
        F = np.array(F_diag).reshape(w_shape)

        if n == 0:
          Fs.append(F)
        else:
          Fs[-1] += F

        print(i, n)

      Fs[-1] /= ewc_n_samples

    return Fs

  def set_fisher(self, f_num=0):
    """ Loads or computes (and saves) the Fisher information matrix for a given
    task and then stores the matrix and previous network's weights as variables.

    Parameters
      f_num: Index of the task to set Fisher values for (starting from 0).
    """
    # try loading matrix
    if os.path.isfile('fisher_' + str(f_num) + '.pckl.gz'):
      print("Loading fisher matrix...")
      sys.stdout.flush()
      with gzip.open('fisher_' + str(f_num) + '.pckl.gz', 'r') as f:
        Fs, F_weights = pickle.load(f)
      b = False
      for w, v in zip(sum(self.prev_network.weights, []), F_weights):
        if not np.array_equal(self.sess.run(w), v):
          b = True
      if b:
        if input("Fisher network weight's are not equal to the previous network weights.\nType 'y' to continue anyway.") != 'y':
          sys.exit()

      maxV = np.max([np.max(Fs[j]) for j in range(len(Fs))])
      minV = np.min([np.min(Fs[j]) for j in range(len(Fs))])
      for i in range(len(Fs)):
        if self.online_ewc:
          Fs[i] = (Fs[i] - minV)/(maxV - minV)
        print(Fs[i].shape, np.mean(Fs[i]))

      if f_num == 0:
        self.Fs1 = Fs
        self.F_weights1 = F_weights
      else:
        self.Fs2 = Fs
        self.F_weights2 = F_weights
      print("done.")
      sys.stdout.flush()
      return

    # otherwise compute and save it
    from experience import Experience
    prev_experience = Experience("NCHW", self.batch_size, self.history_length,
      self.experience_length, self.observation_dims)
    exp_dir = self.load_dir
    if self.prev_load_dir is not None and f_num == 0:
      exp_dir = self.prev_load_dir
    prev_experience.restore(exp_dir)
    Fs = DQN_EWC.calc_fisher(self.sess, self.prev_network, self.s_prev_net, prev_experience, self.ewc_n_samples, self.n_actions)

    print("Saving fisher matrix, do not exit!")
    sys.stdout.flush()
    pickle_vars = [Fs, []]
    for w in sum(self.prev_network.weights, []):
      pickle_vars[-1].append(self.sess.run(w))
    with gzip.open('fisher_' + str(f_num) + '.pckl.gz', 'w') as f:
      pickle.dump(pickle_vars, f, protocol=pickle.HIGHEST_PROTOCOL)

    maxV = np.max([np.max(Fs[j]) for j in range(len(Fs))])
    minV = np.min([np.min(Fs[j]) for j in range(len(Fs))])
    for i in range(len(Fs)):
      if self.online_ewc:
        Fs[i] = (Fs[i] - minV)/(maxV - minV)
      print(Fs[i].shape, np.mean(Fs[i]))

    if f_num == 0:
      self.Fs1 = Fs
      self.F_weights1 = pickle_vars[1]
    else:
      self.Fs2 = Fs
      self.F_weights2 = pickle_vars[1]
    print("done.")
    sys.stdout.flush()

  def setup_train(self, restore):
    """ Setups the agent to be trained.

    Parameters
      restore: Whether to restore from a previous training session (True) or not (False).
    """
    DQN_CF.setup_train(self, restore)

    if not restore:
      # load prev_network weights
      import gzip, pickle
      ws = sum(self.prev_network.weights, [])
      with gzip.open(self.load_dir+'stats.pckl.gz', 'r') as f:
        best_weights = pickle.load(f)[0]
      for i in range(len(ws)):
        self.sess.run(tf.assign(ws[i], best_weights[i]))

    self.set_fisher()

    if self.prev_load_dir is not None:
      self.set_fisher(1)

      if self.online_ewc:
        for i in range(len(self.Fs1)):
          self.Fs1[i] = self.ewc_discount*self.Fs1[i] + self.Fs2[i]
        self.F_weights1 = self.F_weights2

  def update(self):
    """ Updates the predictor network.

    Returns
      List containing the batch's mean Q-value, loss, DQN loss and (online-)EWC loss.
    """
    state_batch, action_tmp, reward_batch, next_state_batch, done_batch = self.experience.sample()

    if self.wm:
      feed_dict = {
        self.s: state_batch,
        self.s_: state_batch
      }
    else:
      feed_dict = {
        self.s: state_batch,
        self.s_: next_state_batch,
        self.terminals: done_batch,
        self.rewards: reward_batch,
        self.actions: action_tmp
      }

    for i in range(len(self.Fs1)):
      feed_dict[self.F_prev_W_placeholders[i]] = self.F_weights1[i]
      feed_dict[self.Fs_placeholders[i]] = self.Fs1[i]

    if self.prev_load_dir is not None and not self.online_ewc:
      for i in range(len(self.Fs2)):
        feed_dict[self.F_prev_W_placeholders[i+len(self.Fs1)]] = self.F_weights2[i]
        feed_dict[self.Fs_placeholders[i+len(self.Fs1)]] = self.Fs2[i]

    q_t, loss, dqn_loss, ewc_loss, _ = self.sess.run([self.pred_network.outputs, self.loss, self.dqn_loss, self.ewc_loss, self.optim], feed_dict=feed_dict)

    return np.mean(q_t), loss, dqn_loss, ewc_loss