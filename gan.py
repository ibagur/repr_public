import tensorflow as tf
import numpy as np
from models import *
import time, sys, os
from session_saver import Session_Saver
import scipy
from ops import reduce_std

class GAN(Session_Saver):
  """ Defines a GAN. """
  def __init__(self, sess, batch_size, in_dims, log, save_dir, gen_only, use_d2=False, scale_d2=1000):
    """ Initialises the GAN.

    Parameters
      sess: Tensorflow session.
      batch_size: Batch size for training and generating.
      in_dims: Input dimensions [channels, height, width].
      log: Logger.
      save_dir: Directory to save to.
      gen_only: Whether to only initialise the generative model (True)
                or not (False).
      use_d2: Whether to use a second discriminator to improve the GAN
              for pseudo-rehearsal (True) or not (False).
      scale_d2: Scaling parameter between the two discriminators (only used when 'use_d2' == True).'
    """
    Session_Saver.__init__(self, sess, save_dir)
    self.batch_size = batch_size
    self.in_dims = in_dims
    self.log = log
    self.use_d2 = use_d2
    self.scale_d2 = scale_d2

    # TODO: have architectures specified by command line arguments
    if len(in_dims) == 1:
      gen_hid_nums = [20, 50]
      discrim_hid_nums = [20, 50]
      self.gen_in_num = 2
    else:
      c_in = in_dims[0]
      data_format = 'NCHW'
      d_filter_nums = [64, 64*2, 64*4]
      d_filter_shapes = [[5, 5], [5, 5], [5, 5]]
      d_filter_strides = [[3, 3], [2, 2], [2, 2]]
      d2_filter_strides = [[1, 1], [1, 1], [1, 1]]

      #g_first_im_shape = [2*64*4, 7, 7]
      #g_filter_nums = [2*64*4, 2*64*2, 2*64, c_in]
      g_first_im_shape = [64*4, 7, 7]
      g_filter_nums = [64*4, 64*2, 64, c_in]
      g_filter_shapes = [[5, 5], [5, 5], [5, 5], [5, 5]]
      g_filter_strides = [[3, 3], [2, 2], [2, 2], [1, 1]]
      self.gen_in_num = 100

    # generator
    self.gen_inputs = tf.placeholder('float32', [batch_size, self.gen_in_num], name='gen_inputs')
    if len(in_dims) == 1:
      self.generator = Generator_MLP(sess, self.gen_inputs, np.prod(in_dims), gen_hid_nums)
    else:
      self.generator = Generator_deCNN(sess, self.gen_inputs, in_dims,
        data_format=data_format, first_im_shape=g_first_im_shape,
        filter_nums=g_filter_nums, filter_shapes=g_filter_shapes,
        filter_strides=g_filter_strides)

    if gen_only:
      return

    self.t_op = tf.Variable(0, trainable=False, name='t_steps')
    self.t_add_op = self.t_op.assign_add(1)

    self.disc_real_inputs = tf.placeholder('float32', [batch_size,] + in_dims, name='disc_real_inputs')

    if self.use_d2:
      d2_layer_i = 1
      d2_noise_scale = 0.33
      self.n_classes = 18
      self.class_network = CNN(self.sess, self.generator.outputs / 2 + 0.5, 'pred_network', self.n_classes, trainable=False,
        data_format=data_format, filter_nums=[32, 64, 64],
        filter_shapes=[[8, 8], [4, 4], [3, 3]], filter_strides=[[4, 4], [2, 2], [1, 1]],
        layer_order=[0,0,0], hid_nums=[512,], reuse=False)
      self.class_network_copy = CNN(self.sess, self.disc_real_inputs/255., 'pred_network', self.n_classes, trainable=False,
        data_format=data_format, filter_nums=[32, 64, 64],
        filter_shapes=[[8, 8], [4, 4], [3, 3]], filter_strides=[[4, 4], [2, 2], [1, 1]],
        layer_order=[0,0,0], hid_nums=[512,], reuse=True)

      self.scaled_disc2_real_inputs = tf.maximum(self.class_network_copy.layer_outs[d2_layer_i] + tf.random_normal(self.class_network_copy.layer_outs[d2_layer_i].shape, 0.0, d2_noise_scale*reduce_std(self.class_network_copy.layer_outs[d2_layer_i], 0)), 0)
      self.discriminator2_real = Discriminator_CNN(sess, self.scaled_disc2_real_inputs,
        data_format=data_format, filter_nums=d_filter_nums,
        filter_shapes=d_filter_shapes, filter_strides=d2_filter_strides, name="discriminator2")

      self.scaled_disc2_fake_inputs = tf.maximum(self.class_network.layer_outs[d2_layer_i] + tf.random_normal(self.class_network.layer_outs[d2_layer_i].shape, 0.0, d2_noise_scale*reduce_std(self.class_network.layer_outs[d2_layer_i], 0)), 0)
      self.discriminator2_fake = Discriminator_CNN(sess, self.scaled_disc2_fake_inputs,
        data_format=data_format, filter_nums=d_filter_nums,
        filter_shapes=d_filter_shapes, filter_strides=d2_filter_strides, reuse=True, name="discriminator2")

    # discriminator real
    if len(in_dims) == 1:
      self.scaled_disc_real_inputs = self.disc_real_inputs+tf.random_uniform(tf.shape(self.disc_real_inputs), -0.1, 0.1)
      self.discriminator_real = Discriminator_MLP(sess, self.scaled_disc_real_inputs, discrim_hid_nums)
    else:
      self.scaled_disc_real_inputs = 2.*((self.disc_real_inputs + tf.random_uniform(tf.shape(self.disc_real_inputs), -10, 10)) / 255. - 0.5)
      self.discriminator_real = Discriminator_CNN(sess, self.scaled_disc_real_inputs,
        data_format=data_format, filter_nums=d_filter_nums,
        filter_shapes=d_filter_shapes, filter_strides=d_filter_strides)

    # discriminator fake
    self.disc_fake_inputs = self.generator.outputs
    if len(in_dims) == 1:
      self.scaled_disc_fake_inputs = self.disc_fake_inputs+tf.random_uniform(tf.shape(self.disc_fake_inputs), -0.1, 0.1)
      self.discriminator_fake = Discriminator_MLP(sess, self.scaled_disc_fake_inputs, discrim_hid_nums, reuse=True)
    else:
      self.scaled_disc_fake_inputs = 2.*((255*(self.disc_fake_inputs / 2 + 0.5) + tf.random_uniform(tf.shape(self.disc_fake_inputs), -10, 10)) / 255 - 0.5)
      self.discriminator_fake = Discriminator_CNN(sess, self.scaled_disc_fake_inputs,
        data_format=data_format, filter_nums=d_filter_nums,
        filter_shapes=d_filter_shapes, filter_strides=d_filter_strides, reuse=True)

    t_vars = tf.trainable_variables()
    a = [var for var in t_vars if 'discriminator' in var.name]
    for i in a:
      print(i)
    print()
    a = [var for var in t_vars if 'generator' in var.name]
    for i in a:
      print(i)

    disc_real = self.discriminator_real.outputs
    disc_fake = self.discriminator_fake.outputs

    # WGAN loss
    gen_cost = -tf.reduce_mean(disc_fake)
    disc_cost = tf.reduce_mean(disc_fake) - tf.reduce_mean(disc_real)

    self.g_loss = tf.reduce_mean(tf.cast(disc_fake < 0, tf.float32))
    self.d_loss = [tf.reduce_mean(tf.cast(disc_real < 0, tf.float32)), tf.reduce_mean(tf.cast(disc_fake >= 0, tf.float32))]

    # Gradient penalty
    alpha = tf.random_uniform(
        shape=[self.batch_size, ],
        minval=0.,
        maxval=1.
    )

    # note: this is how WGAN-GP paper computed: interpolates = e*real_ims+(1-e)*fake_ims (i.e. more efficient than the equation given)
    differences = self.scaled_disc_fake_inputs - self.scaled_disc_real_inputs
    interpolates = self.scaled_disc_real_inputs + tf.transpose(tf.multiply(tf.transpose(differences), alpha))

    if len(in_dims) == 1:
      discriminator_interpolates = Discriminator_MLP(sess, interpolates, discrim_hid_nums, reuse=True)
    else:
      discriminator_interpolates = Discriminator_CNN(sess, interpolates,
        data_format=data_format, filter_nums=d_filter_nums,
        filter_shapes=d_filter_shapes, filter_strides=d_filter_strides, reuse=True)

    LAMBDA = 10

    gradients = tf.gradients(discriminator_interpolates.outputs, [interpolates])[0]
    slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1]))
    gradient_penalty = tf.reduce_mean((slopes-1.)**2)
    disc_cost += LAMBDA*gradient_penalty

    # discriminator output regularised
    disc_cost += 0.000001*tf.reduce_mean(tf.square(disc_real))
    disc_cost += 0.000001*tf.reduce_mean(tf.square(disc_fake))

    self.d_loss_opt = disc_cost
    self.g_loss_opt = gen_cost

    if self.use_d2:
      disc2_real = self.discriminator2_real.outputs
      disc2_fake = self.discriminator2_fake.outputs

      # WGAN loss
      gen_cost_d2 = -tf.reduce_mean(disc2_fake)
      disc2_cost = tf.reduce_mean(disc2_fake) - tf.reduce_mean(disc2_real)

      self.g_loss_d2 = tf.reduce_mean(tf.cast(disc2_fake < 0, tf.float32))
      self.d2_loss = [tf.reduce_mean(tf.cast(disc2_real < 0, tf.float32)), tf.reduce_mean(tf.cast(disc2_fake >= 0, tf.float32))]

      # Gradient penalty
      alpha_d2 = tf.random_uniform(
          shape=[self.batch_size, ],
          minval=0.,
          maxval=1.
      )

      # note: this is how WGAN-GP paper computed: interpolates = e*real+(1-e)*fake (i.e. more efficient than the equation given)
      differences_d2 = self.scaled_disc2_fake_inputs - self.scaled_disc2_real_inputs
      interpolates_d2 = self.scaled_disc2_real_inputs + tf.transpose(tf.multiply(tf.transpose(differences_d2), alpha_d2))

      discriminator2_interpolates = Discriminator_CNN(sess, interpolates_d2,
        data_format=data_format, filter_nums=d_filter_nums,
        filter_shapes=d_filter_shapes, filter_strides=d2_filter_strides, reuse=True, name="discriminator2")

      gradients_d2 = tf.gradients(discriminator2_interpolates.outputs, [interpolates_d2])[0]
      slopes_d2 = tf.sqrt(tf.reduce_sum(tf.square(gradients_d2), reduction_indices=[1]))
      gradient_penalty_d2 = tf.reduce_mean((slopes_d2-1.)**2)
      disc2_cost += LAMBDA*gradient_penalty_d2

      # discriminator output regularised
      disc2_cost += 0.000001*tf.reduce_mean(tf.square(disc2_real))
      disc2_cost += 0.000001*tf.reduce_mean(tf.square(disc2_fake))

      self.d2_loss_opt = disc2_cost
      self.g_loss_opt_d2 = gen_cost_d2

    # optimisers
    var_list = sum(self.generator.weights, [])
    if self.use_d2:
      self.g_optim = tf.train.AdamOptimizer(0.001, beta1=0., beta2=0.99).minimize(gen_cost+self.scale_d2*gen_cost_d2, var_list=var_list)
      var_list = sum(self.discriminator_real.weights, []) + sum(self.discriminator2_real.weights, [])
      self.d_optim = tf.train.AdamOptimizer(0.001, beta1=0., beta2=0.99).minimize(disc_cost+self.scale_d2*disc2_cost, var_list=var_list)
    else:
      self.g_optim = tf.train.AdamOptimizer(0.001, beta1=0., beta2=0.99).minimize(gen_cost, var_list=var_list)
      var_list = sum(self.discriminator_real.weights, [])
      self.d_optim = tf.train.AdamOptimizer(0.001, beta1=0., beta2=0.99).minimize(disc_cost, var_list=var_list)


  def to_image(self, raw_im):
    """Converts a generated single frame/image into RGB uint8 format.

    Parameters
      raw_im: Single frame/image to convert [height, width].

    Returns
      Converted frame/image [height, width, channels].
    """
    im = np.array([raw_im, raw_im, raw_im]).transpose((1, 2, 0))
    return np.maximum(0, np.minimum(255, im)).astype(np.uint8)

  def to_image_array(self, raw_ims):
    """ Converts an array of generated frames/images into a "2D" array for
    displaying. This array is 5 frames/images high and wide.

    Parameters
      raw_ims: Frames/images to convert into "2D" array.

    Returns
      Converted array of frames/images [height, width, channels].
    """
    im_h, im_w = raw_ims.shape[1:3]
    n_h = 5
    n_w = 5
    result = np.zeros((n_h*im_h+n_h+1, n_w*im_w+n_w+1, 3))
    result[:,:,0] = 255
    count = 0
    for h in range(n_h):
      for w in range(n_w):
        result[h*im_h+h+1:(h+1)*im_h+h+1, w*im_w+w+1:(w+1)*im_w+w+1] = self.to_image(raw_ims[count])
        count += 1
    return result

  def train(self, dataset, t_max, pr, ratio, restore=False, gan_load_dir=None):
    """ Trains the GAN.

    Parameters
      dataset: Experience replay to draw sequences from.
      t_max: Total number of updates to make to both the generator and
             the discriminator while training.
      pr: Whether training should be done with sequences from the experience replay
          (True) or both the experience replay and pseudo-items (False).
      ratio: Ratio between new task sequences (from the experience replay) and
             previously generated sequences (only applied when 'pr' == True).
      restore: Whether to restore a previous training session (True) or not (False).
      gan_load_dir: Directory to load the previous GAN's pseudo-items from.
                    DQN also loaded from here if 'use_d2' == True.
    """
    self.experience = dataset

    if pr:
      self.pseudo_ims = np.load(gan_load_dir+'pseudo_ims.npz')['arr_0']
      self.pseudo_idxs = np.random.permutation(len(self.pseudo_ims))
      self.pseudo_cur_i = 0
      self.pr_batch_size = self.batch_size - int(self.batch_size*ratio)

    print_hz = 500
    self.sess.run(tf.global_variables_initializer())

    if restore:
      self.restore()
    elif self.use_d2:
      import gzip, pickle
      ws = sum(self.class_network.weights, [])
      with gzip.open(gan_load_dir+'stats.pckl.gz', 'r') as f:
        best_weights = pickle.load(f)[0]
      for i in range(len(ws)):
        # to be used if using AC + GRIm-RePR
        #self.sess.run(tf.assign(ws[i], best_weights[i + (0 if i < 6 else 4)]))
        self.sess.run(tf.assign(ws[i], best_weights[i]))
      #sess_save = Session_Saver(self.sess, gan_load_dir)
      #sess_save.restore(sum(self.class_network.weights, []))
    # uncomment for restoring just generator when training next gan
    #saver = tf.train.Saver(sum(self.generator.weights, []))
    #saver.restore(self.sess, "load_gan_save_folder/checkpoint.ckpt")

    # uncomment for restoring all params when training next gan
    #saver = tf.train.Saver()
    #saver.restore(self.sess, "load_gan_save_folder/checkpoint.ckpt")
    #self.sess.run(self.t_op.initializer)

    directory = self.save_dir + "images/"
    if not os.path.exists(directory):
      os.makedirs(directory)
    batch_images = self.experience.sample()[0]

    if pr:
      batch_idxs = self.pseudo_idxs[self.pseudo_cur_i:self.pseudo_cur_i+self.pr_batch_size]
      pseudo_ims = self.pseudo_ims[batch_idxs]

      batch_images = np.concatenate([batch_images, pseudo_ims], 0)
      batch_images = batch_images[np.random.permutation(len(batch_images))]
    scipy.misc.imsave('%sreal_im_t%d.png' % (directory, 0), self.to_image_array(batch_images[:,-3,:,:]))
    scipy.misc.imsave('%sreal_im_t%d.png' % (directory, 1), self.to_image_array(batch_images[:,-2,:,:]))
    scipy.misc.imsave('%sreal_im_t%d.png' % (directory, 2), self.to_image_array(batch_images[:,-1,:,:]))

    # train
    self.t = self.t_op.eval(session=self.sess)
    self.log.print("training...")
    prev_time = time.time()
    while self.t < t_max:
      msg = "iter: %d / %d" % (self.t, t_max)
      sys.stdout.write(msg + chr(8) * len(msg))
      sys.stdout.flush()

      # Update D network
      for i in range(1):
        batch_images = self.experience.sample()[0]

        if pr:
          if self.pseudo_cur_i + self.pr_batch_size >= len(self.pseudo_ims):
            self.pseudo_idxs = np.random.permutation(len(self.pseudo_ims))
            self.pseudo_cur_i = 0

          batch_idxs = self.pseudo_idxs[self.pseudo_cur_i:self.pseudo_cur_i+self.pr_batch_size]
          pseudo_ims = self.pseudo_ims[batch_idxs]

          batch_images = np.concatenate([batch_images, pseudo_ims], 0)

          self.pseudo_cur_i += self.pr_batch_size

        gen_ins = np.random.uniform(-1, 1, [self.batch_size, self.gen_in_num])
        self.sess.run(self.d_optim, feed_dict={self.disc_real_inputs: batch_images, self.gen_inputs: gen_ins})

      # Update G network
      gen_ins = np.random.uniform(-1, 1, [self.batch_size, self.gen_in_num])
      self.sess.run(self.g_optim, feed_dict={self.gen_inputs: gen_ins})

      if (self.t + 1) % print_hz == 0 or self.t == 0:
        batch_images = self.experience.sample()[0]

        if pr:
          if self.pseudo_cur_i + self.pr_batch_size >= len(self.pseudo_ims):
            self.pseudo_idxs = np.random.permutation(len(self.pseudo_ims))
            self.pseudo_cur_i = 0

          batch_idxs = self.pseudo_idxs[self.pseudo_cur_i:self.pseudo_cur_i+self.pr_batch_size]
          pseudo_ims = self.pseudo_ims[batch_idxs]

          batch_images = np.concatenate([batch_images, pseudo_ims], 0)

          self.pseudo_cur_i += self.pr_batch_size

        gen_ins = np.random.uniform(-1, 1, [self.batch_size, self.gen_in_num])
        g_loss = self.sess.run(self.g_loss, feed_dict={self.gen_inputs: gen_ins})
        d_loss = self.sess.run(self.d_loss, feed_dict={self.disc_real_inputs: batch_images, self.gen_inputs: gen_ins})
        g_loss_opt = self.sess.run(self.g_loss_opt, feed_dict={self.gen_inputs: gen_ins})
        d_loss_opt = self.sess.run(self.d_loss_opt, feed_dict={self.disc_real_inputs: batch_images, self.gen_inputs: gen_ins})
        if self.use_d2:
          g_loss_d2 = self.sess.run(self.g_loss_d2, feed_dict={self.gen_inputs: gen_ins})
          d2_loss = self.sess.run(self.d2_loss, feed_dict={self.disc_real_inputs: batch_images, self.gen_inputs: gen_ins})
          g_loss_opt_d2 = self.sess.run(self.g_loss_opt_d2, feed_dict={self.gen_inputs: gen_ins})
          d2_loss_opt = self.sess.run(self.d2_loss_opt, feed_dict={self.disc_real_inputs: batch_images, self.gen_inputs: gen_ins})

        self.log.print("\ntime: %.2fm" % ((time.time()-prev_time)/60))
        prev_time = time.time()
        self.log.print("t: %d, d_loss_real: %.2f, d_loss_fake: %.2f, g_loss: %.2f" % (self.t, d_loss[0], d_loss[1], g_loss))
        if self.use_d2:
          self.log.print("d2_loss_real: %.2f, d2_loss_fake: %.2f, g_loss_d2: %.2f" % (d2_loss[0], d2_loss[1], g_loss_d2))
        self.log.print("d_loss_opt: %.2f, g_loss_opt: %.2f" % (d_loss_opt, g_loss_opt))
        if self.use_d2:
          self.log.print("d2_loss_opt: %.2f, g_loss_opt_d2: %.2f" % (d2_loss_opt, g_loss_opt_d2))
        fake_outs = self.sess.run(self.discriminator_fake.outputs, feed_dict={self.gen_inputs: gen_ins})
        real_outs = self.sess.run(self.discriminator_real.outputs, feed_dict={self.disc_real_inputs: batch_images})
        self.log.print("real_min: %.2f, real_max: %.2f, fake_min: %.2f, fake_max: %.2f" %
          (np.min(real_outs), np.max(real_outs), np.min(fake_outs), np.max(fake_outs)))
        if self.use_d2:
          fake_outs_d2 = self.sess.run(self.discriminator2_fake.outputs, feed_dict={self.gen_inputs: gen_ins})
          real_outs_d2 = self.sess.run(self.discriminator2_real.outputs, feed_dict={self.disc_real_inputs: batch_images})
          self.log.print("real_min_d2: %.2f, real_max_d2: %.2f, fake_min_d2: %.2f, fake_max_d2: %.2f" %
            (np.min(real_outs_d2), np.max(real_outs_d2), np.min(fake_outs_d2), np.max(fake_outs_d2)))

        gen_ims = self.gen()
        scipy.misc.imsave('%sgen_im%d_t%d.png' % (directory, self.t//print_hz, 0), self.to_image_array(gen_ims[:,-3,:,:]))
        scipy.misc.imsave('%sgen_im%d_t%d.png' % (directory, self.t//print_hz, 1), self.to_image_array(gen_ims[:,-2,:,:]))
        scipy.misc.imsave('%sgen_im%d_t%d.png' % (directory, self.t//print_hz, 2), self.to_image_array(gen_ims[:,-1,:,:]))

        self.t += 1
        self.sess.run(self.t_add_op)
        self.checkpoint()

        self.log.print()
      else:
        self.t += 1
        self.sess.run(self.t_add_op)

  def restore_gen_only(self):
    """ Restores only the generative model. """
    gen_vars = sum(self.generator.weights, [])
    self.restore(gen_vars)

  def gen(self):
    """ Generates a batch of sequences.

    Returns
      Batch of generated uint8 sequences [batch, channels, height, width].
    """
    gen_ins = np.random.uniform(-1, 1, [self.batch_size, self.gen_in_num])
    gen_ims = self.sess.run(self.generator.outputs, feed_dict={self.gen_inputs: gen_ins})
    if len(self.in_dims) != 1:
      gen_ims = 255*(gen_ims/2+0.5)
    return gen_ims.astype(np.uint8)

def lrelu(x, leak=0.2, name="lrelu"):
  """ Applies a leaky ReLU activation function to the input, returning the result.

  Parameters
    x: Input to the function.
    leak: Leakage factor.
    name: Name of function.

  Returns
    Leaky ReLU's output.
  """
  return tf.maximum(x, leak*x)

class Discriminator_MLP(MLP):
  """ Defines a MLP discriminator. """
  def __init__(self, sess, inputs, hid_nums, trainable=True, reuse=False):
    """ Initialises the discriminator.

    Parameters
      sess: Tensorflow session.
      inputs: Input tensor.
      hid_nums: Number of units in each of the network's hidden layers.
      trainable: Whether the network's parameters are trainable (True) or not (False).
      reuse: Whether to reuse an existing network's weight tensors (True) or not (False).
    """
    MLP.__init__(self, sess, inputs, "discriminator", 1, trainable, reuse,
      hid_nums, activation=lrelu)

class Generator_MLP(MLP):
  """ Defines a MLP generator. """
  def __init__(self, sess, inputs, n_outputs, hid_nums, trainable=True, reuse=False):
    """ Initialises the generator.

    Parameters
      sess: Tensorflow session.
      inputs: Input tensor.
      n_outputs: Number of outputs to generate values for.
      hid_nums: Number of units in each of the network's hidden layers.
      trainable: Whether the network's parameters are trainable (True) or not (False).
      reuse: Whether to reuse an existing network's weight tensors (True) or not (False).
    """
    MLP.__init__(self, sess, inputs, "generator", n_outputs, trainable, reuse,
      hid_nums)

class Discriminator_CNN(CNN):
  """ Defines a CNN discriminator. """
  def __init__(self, sess, inputs, data_format, filter_nums, filter_shapes,
    filter_strides, trainable=True, reuse=False, name="discriminator"):
    """ Initialises the discriminator.

    Parameters
      sess: Tensorflow session.
      inputs: Input tensor.
      data_format: Format to expect images/sequences in. Valid values are ['NHWC', 'NCHW'].
      filter_nums: Number of filters in the network.
      filter_shapes: Shape of filters in the network.
      filter_strides: Strides of filters in the network.
      trainable: Whether the network's parameters are trainable (True) or not (False).
      reuse: Whether to reuse an existing network's weight tensors (True) or not (False).
      name: "Name of the discriminator."
    """
    CNN.__init__(self, sess, inputs, name, 1, trainable=trainable,
      reuse=reuse, data_format=data_format, filter_nums=filter_nums,
      filter_shapes=filter_shapes, filter_strides=filter_strides, filter_padding='SAME',
      layer_order=[0 for _ in range(len(filter_nums))], hid_nums=[], activation=lrelu)

class Generator_deCNN(Model):
  """ Defines a CNN generator. """
  def __init__(self, sess, inputs, n_outputs, data_format, first_im_shape,
    filter_nums, filter_shapes, filter_strides, trainable=True, reuse=False,
    bn_epsilon=1e-5, bn_momentum=0.9):
    """ Initialises the generator.

    Parameters
      sess: Tensorflow session.
      inputs: Input tensor.
      n_outputs: Number of outputs to generate values for.
      data_format: Format to expect images/sequences in. Valid values are ['NHWC', 'NCHW'].
      first_im_shape: Shape of the first sequence/image passed through the deconvolutional
                      layers. This is created by a fully collected layer from the
                      input/latent variables to this image [channels, height, width].
      filter_nums: Number of filters in the network.
      filter_shapes: Shape of filters in the network.
      filter_strides: Strides of filters in the network.
      trainable: Whether the network's parameters are trainable (True) or not (False).
      reuse: Whether to reuse an existing network's weight tensors (True) or not (False).
      bn_epsilon: Epsilon value for batch normalisation.
      bn_momentum: Momentum value for batch normalisation.
    """
    assert(len(filter_nums) == len(filter_shapes)), "The filter_nums must equal the number of filter_shapes in the deCNN."
    assert(len(filter_nums) == len(filter_strides)), "The filter_nums must equal the number of filter_strides in the deCNN."
    self.data_format = data_format
    self.first_im_shape = first_im_shape
    self.filter_nums = filter_nums
    self.filter_shapes = filter_shapes
    self.filter_strides = filter_strides
    self.bn_epsilon = bn_epsilon
    self.bn_momentum = bn_momentum
    Model.__init__(self, sess, inputs, "generator", n_outputs, trainable, reuse)

  def init_model(self):
    """ Initialises the deCNN. """
    weights = []
    layer_out = self.inputs

    with tf.variable_scope(self.name) as scope:
      if self.reuse:
        scope.reuse_variables()

      layer_out, Ws = linear("fc-%d"%0, layer_out, int(np.prod(self.first_im_shape)),
        trainable=self.trainable)
      layer_out = tf.contrib.layers.batch_norm(layer_out, decay=self.bn_momentum,
        updates_collections=None, epsilon=self.bn_epsilon, scale=True, is_training=True,
        scope="fc_batch_norm_%d"%0, data_format=self.data_format)
      Ws.extend(tf.trainable_variables()[-2:])
      layer_out = tf.nn.relu(layer_out)
      print(layer_out)
      print(Ws)
      print()
      weights.append(Ws)

      layer_out = tf.reshape(layer_out, [tf.shape(self.inputs)[0]]+self.first_im_shape)

      for i in range(len(self.filter_nums)):
        layer_out, Ws = deconv2d("deconv-%d"%i, layer_out,
          self.filter_nums[i], k_h=self.filter_shapes[i][0],
          k_w=self.filter_shapes[i][1], s_h=self.filter_strides[i][0],
          s_w=self.filter_strides[i][1], trainable=self.trainable,
          data_format=self.data_format, padding="SAME")
        if i < len(self.filter_nums)-1:
          layer_out = tf.contrib.layers.batch_norm(layer_out, decay=self.bn_momentum,
            updates_collections=None, epsilon=self.bn_epsilon, scale=True, is_training=True,
            scope="conv_batch_norm_%d"%i, data_format=self.data_format)
          Ws.extend(tf.trainable_variables()[-2:])
          layer_out = tf.nn.relu(layer_out)
        print(layer_out)
        print(Ws)
        print()
        weights.append(Ws)


      self.outputs = tf.nn.tanh(layer_out)
      self.weights = weights