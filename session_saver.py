import tensorflow as tf

class Session_Saver:
  """ Defines functions for saving and restoring a tensorflow session. """

  def __init__(self, sess, save_dir):
    """ Initialise the session saver.

    Parameters
      sess: Tensorflow session.
      save_dir: Directory to save to.
    """
    self.sess = sess
    self.save_dir = save_dir

  def restore(self, var_list=None):
    """ Restores a past tensorflow session.

    Parameters
      var_list: List of Tensorflow variables to restore. If None, all variables are
                restored.
    """
    saver = tf.train.Saver(var_list=var_list)
    saver.restore(self.sess, self.save_dir+"checkpoint.ckpt")

  def checkpoint(self):
    """ Saves the current tensorflow session. """
    saver = tf.train.Saver()
    saver.save(self.sess, self.save_dir+"checkpoint.ckpt")