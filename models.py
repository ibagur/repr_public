from ops import *

class Model():
  """ Defines the main functions of a model. This class contains methods that a
  child class must override. """
  def __init__(self, sess, inputs, name, n_outputs, trainable, reuse):
    """ Initialises the model.

    Parameters
      sess: Tensorflow session.
      inputs: Input tensor.
      name: Name of the model.
      n_outputs: Number of output units in the model.
      trainable: Whether the model's parameters are trainable (True) or not (False).
      reuse: Whether to reuse an existing model's weight tensors (True) or not (False).
    """
    self.sess = sess
    self.inputs = inputs
    self.name = name
    self.n_outputs = n_outputs
    self.trainable = trainable
    self.reuse = reuse
    self.copy_op = None
    self.assign_op = None
    self.init_model()
    self.create_assign_op()

  def init_model(self):
    """ Initialises the model, setting its weights and outputs.

    *************************************
    * This method should be overridden. *
    *************************************
    """
    raise NotImplementedError

  def create_assign_op(self):
    """ Creates an operation for assigning the model's weights with specified
    values. """
    self.assign_in = tf.placeholder('float32', name='assign_in')
    self.assign_op = []
    weights = sum(self.weights, [])
    for i in range(len(weights)):
      self.assign_op.append(tf.assign(weights[i], self.assign_in))

  def load_weights(self, new_weights):
    """ Sets the model's weights to the inputted values.

    Parameters
      new_weights: List of weights/values to set the current model to.
    """
    if self.assign_op is None:
      raise Exception("run `create_assign_op` first before 'load_weights'")
    else:
      new_weights = sum(new_weights, [])
      assert(len(new_weights) == len(self.assign_op)), "There is not the same number of variables (weights) being loaded as there are in the model."
      for i in range(len(new_weights)):
        self.sess.run(self.assign_op[i], feed_dict={self.assign_in: new_weights[i]})

  def create_copy_op(self, model):
    """ Creates an operation for assigning the current model's weights with the
    weights in the model specified.

    Parameters
      model: Model to copy weights from.
    """
    copy_ops = []
    weights = sum(self.weights, [])
    weights_to_copy = sum(model.weights, [])

    assert(len(weights) == len(weights_to_copy)), "There is not the same number of variables (weights_to_copy) being loaded as there are in the model (weights)."

    for i in range(len(weights)):
      copy_op = weights[i].assign(weights_to_copy[i])
      copy_ops.append(copy_op)

    self.copy_op = tf.group(*copy_ops, name='copy_op')

  def run_copy(self):
    """ Sets the current model's weights to the weights of the model the operation
    has been created for. """
    if self.copy_op is None:
      raise Exception("run `create_copy_op` first before 'run_copy'")
    else:
      self.sess.run(self.copy_op)

class MLP(Model):
  """ Defines a MLP network. """
  def __init__(self, sess, inputs, name, n_outputs, trainable=True, reuse=False,
    hid_nums=[], weights_initializer=tf.contrib.layers.xavier_initializer(),
    biases_initializer=tf.constant_initializer(0.1), activation=tf.nn.relu):
    """ Initialises the MLP network.

    Parameters
      sess: Tensorflow session.
      inputs: Input tensor.
      name: Name of network.
      n_outputs: Number of output units in the network.
      trainable: Whether the network's parameters are trainable (True) or not (False).
      reuse: Whether to reuse an existing network's weight tensors (True) or not (False).
      hid_nums: Number of units in each of the network's hidden layers.
      weights_initializer: Initialiser for the weight tensors.
      biases_initializer: Initialiser for the bias tensors.
      activation: Activation used by every layer in the network except the last.
    """
    self.hid_nums = hid_nums
    self.weights_initializer = weights_initializer
    self.biases_initializer = biases_initializer
    self.activation = activation
    Model.__init__(self, sess, inputs, name, n_outputs, trainable, reuse)

  def init_model(self):
    """ Initialises the MLP network, setting its weights and outputs. """
    weights = []
    layer_outs = []
    layer_out = self.inputs

    # Flatten
    layer_out = tf.contrib.layers.flatten(layer_out)

    with tf.variable_scope(self.name) as scope:
      if self.reuse:
        scope.reuse_variables()

      # FC layer
      for i in range(len(self.hid_nums)):
        layer_out, Ws = linear("fc-%d"%i, layer_out, self.hid_nums[i],
          trainable=self.trainable, weights_initializer=self.weights_initializer,
          biases_initializer=self.biases_initializer)
        print(layer_out)
        print(Ws)
        print()
        layer_out = self.activation(layer_out)
        layer_outs.append(layer_out)
        weights.append(Ws)

      # Linear layer
      layer_out, Ws = linear("out", layer_out, self.n_outputs, trainable=self.trainable,
        weights_initializer=self.weights_initializer, biases_initializer=self.biases_initializer)
      print(layer_out)
      print(Ws)
      print()
      layer_outs.append(layer_out)
      weights.append(Ws)

      self.outputs = layer_out
      self.weights = weights
      self.layer_outs = layer_outs

class CNN(Model):
  """ Defines a CNN. """
  def __init__(self, sess, inputs, name, n_outputs, trainable=True, reuse=False,
    data_format='NHWC', filter_nums=[16, 32], filter_shapes=[[3, 3], [3, 3]],
    filter_strides=[[1, 1], [1, 1]], filter_padding='VALID', pool_shapes=[],
    pool_strides=[], pool_padding='VALID', layer_order=[0, 0], hid_nums=[128, 64],
    weights_initializer=tf.contrib.layers.xavier_initializer(),
    biases_initializer=tf.constant_initializer(0.1), activation=tf.nn.relu):
    """ Initialises the CNN.

    Parameters
      sess: Tensorflow session.
      inputs: Input tensor.
      name: Name of network.
      n_outputs: Number of output units in the network.
      trainable: Whether the network's parameters are trainable (True) or not (False).
      reuse: Whether to reuse an existing network's weight tensors (True) or not (False).
      data_format: Format to expect images/sequences in. Valid values are ['NHWC', 'NCHW'].
      filter_nums: Number of filters in the network.
      filter_shapes: Shape of filters in the network.
      filter_strides: Strides of filters in the network.
      filter_padding: Padding of filters in the network.
      pool_shapes: Shape of max-pooling kernels in the network.
      pool_strides: Strides of max-pooling kernels in the network.
      pool_padding: Padding of max-pooling kernels in the network.
      layer_order: Order of the convolutional and max-pooling layers in the
                   network (0 = convolutional, 1 = max-pooling).
      hid_nums: Number of units in each of the network's hidden layers.
      weights_initializer: Initialiser for the weight tensors.
      biases_initializer: Initialiser for the bias tensors.
      activation: Activation used by every layer in the network except the last.
    """
    self.data_format = data_format
    num_conv = layer_order.count(0)
    num_pool = layer_order.count(1)
    assert(len(filter_nums) == num_conv), "filter_nums must equal the number of 0's (conv layers) in layer_order."
    assert(len(filter_shapes) == num_conv), "filter_shapes must equal the number of 0's (conv layers) in layer_order."
    assert(len(filter_strides) == num_conv), "filter_strides must equal the number of 0's (conv layers) in layer_order."
    assert(len(pool_shapes) == num_pool), "pool_shapes must equal the number of 1's (max-pooling layers) in layer_order."
    assert(len(pool_strides) == num_pool), "pool_strides must equal the number of 1's (max-pooling layers) in layer_order."
    self.filter_nums = filter_nums
    self.filter_shapes = filter_shapes
    self.filter_strides = filter_strides
    self.filter_padding = filter_padding
    self.pool_shapes = pool_shapes
    self.pool_strides = pool_strides
    self.pool_padding = pool_padding
    self.layer_order = layer_order
    self.hid_nums = hid_nums
    self.weights_initializer = weights_initializer
    self.biases_initializer = biases_initializer
    self.activation = activation
    Model.__init__(self, sess, inputs, name, n_outputs, trainable, reuse)

  def init_model(self):
    """ Initialises CNN, setting its weights and outputs. """
    weights = []
    layer_outs = []
    layer_out = self.inputs

    with tf.variable_scope(self.name) as scope:
      if self.reuse:
        scope.reuse_variables()

      conv_count = 0
      pool_count = 0
      for layer in self.layer_order:
        # Conv layer
        if layer == 0:
          layer_out, Ws = conv2d("conv-%d"%conv_count, layer_out,
            self.filter_nums[conv_count], k_h=self.filter_shapes[conv_count][0],
            k_w=self.filter_shapes[conv_count][1], s_h=self.filter_strides[conv_count][0],
            s_w=self.filter_strides[conv_count][1], trainable=self.trainable,
            data_format=self.data_format, padding=self.filter_padding,
            weights_initializer=self.weights_initializer, biases_initializer=self.biases_initializer)
          print(layer_out)
          print(Ws)
          print()
          layer_out = self.activation(layer_out)
          layer_outs.append(layer_out)
          weights.append(Ws)
          conv_count += 1
        # Maxpool layer
        elif layer == 1:
          layer_out = maxpool("maxpool-%d"%pool_count, layer_out, k_h=self.pool_shapes[pool_count][0],
            k_w=self.pool_shapes[pool_count][1], s_h=self.pool_strides[pool_count][0],
            s_w=self.pool_strides[pool_count][1], data_format=self.data_format,
            padding=self.pool_padding)
          print(layer_out)
          print()
          pool_count += 1

      # Flatten
      layer_out = tf.contrib.layers.flatten(layer_out)

      # FC layer
      for i in range(len(self.hid_nums)):
        layer_out, Ws = linear("fc-%d"%i, layer_out, self.hid_nums[i],
          trainable=self.trainable, weights_initializer=self.weights_initializer,
          biases_initializer=self.biases_initializer)
        print(layer_out)
        print(Ws)
        print()
        layer_out = self.activation(layer_out)
        layer_outs.append(layer_out)
        weights.append(Ws)

      if self.n_outputs is not None:
        # Linear layer
        layer_out, Ws = linear("out", layer_out, self.n_outputs, trainable=self.trainable,
          weights_initializer=self.weights_initializer, biases_initializer=self.biases_initializer)
        print(layer_out)
        print(Ws)
        print()
        layer_outs.append(layer_out)
        weights.append(Ws)

      self.outputs = layer_out
      self.weights = weights
      self.layer_outs = layer_outs
