import sys
import os

class Logger():
  """ Defines a class used for logging. This class prints to stdout and a log file
  (if specified). """

  def __init__(self, log_dir, log_name):
    """ Initialises logger by creating a log file (if specified).

    Parameters
      log_dir: Path to the directory to create the log file in. If None, no log
               file will be created and thus printing will only be done to stdout.
      log_name: Name of log file to create ('.txt' will be added to the end).
    """
    self.log_name = log_name
    if log_dir != None:
      self.log_path = log_dir + log_name + ".txt"
      self.log_file = open(self.log_path, 'a+')
    else:
      self.log_path = None

  def print(self, *args):
    """ Writes the values in args to the log file (if one exists) and then prints them
    to stdout.

    Parameters
      args: Arguments will be printed and written to log file. Spaces
            are put between each argument.
    """
    s = []
    for v in args:
      s.append(str(v))
    s = " ".join(s)
    if self.log_path != None:
      self.log_file.write(s+"\n")
      self.log_file.flush()
    print(("[%s] "%self.log_name)+s)
    sys.stdout.flush()

  def close_log(self):
    """ Closes the log file if it exists. """
    if self.log_path != None:
      self.log_file.close()
      self.log_path = None