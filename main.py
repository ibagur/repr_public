import os, sys
import tensorflow as tf
from atari_env import AtariEnv
import numpy as np
import ast

# Load default values for command line arguments from .json
import json
config_path = 'config/default.json'
if len(sys.argv) > 1 and sys.argv[1][-5:] == '.json':
  config_path = sys.argv[1]
defaults = json.load(open(config_path))

# Setup command line arguments using the defaults loaded above and descriptions below
flags = tf.app.flags
flags_d = {
  'env_name': 'Name of environment.',

  'save_dir': 'Directory to make saves in.',
  "stats_log_hz": 'Frequency to record stats.',
  "checkpoint_hz": 'Frequency to checkpoint session.',
  "test_hz": 'Frequency to record test results.',
  "seed": 'Seed used for training/testing.',

  "ep_start": 'Starting exploration rate.',
  "ep_end": 'Final exploration rate.',
  "t_ep_end": 'Timestep to linearly decay the exploration rate until.',
  "test_ep": 'Probability of a random action during testing.',

  "observation_dims": "Shape of the frames inputted [height, width].",
  "history_length": 'The number of consecutive frames in an inputted state.',
  "experience_length": 'Number of frames to store in the experience replay.',

  'filter_nums': 'Number of filters in the network.',
  'filter_shapes': 'Shape of filters in the network.',
  'filter_strides': 'Strides of filters in the network.',
  'layer_order': 'Order of layers in the network.',
  'hid_nums': "Number of units in each of the network's hidden layers.",

  "t_train_max": 'Number of timesteps to train until.',
  "t_learn_start": 'Number of timesteps that occur before network updates begin.',
  "update_hz": 'The frequency the predictor network is updated at.',
  "update_target_hz": 'The frequency the target network is updated at.',

  "batch_size": 'Batch size used during training.',
  "discount_r": 'Discount to apply to future rewards.',
  "learning_rate": "Optimiser's learning rate.",
  "momentum": "Optimiser's momentum.",
  "decay": "Optimiser's decay.",
  "epsilon": "Optimiser's epsilon.",

  "train": 'Whether the network is training (True) or testing (False).',
  "mode": "What mode to run in ['std', 'cf', 'reh', 'pr', 'ewc', 'F_overlap']",
  "display": 'Whether to display the testing environment (True) or not (False).',
  "restore": 'Whether to restore training (True) or not (False).',

  "prev_env_name": 'List of previously learnt environment names (oldest to newest).',
  "load_dir": "Directory to load the previous environment's network and experience from.",
  "partial_load": "Whether to exclude the linear layers when loading the pred_network (True) or not (False). When the number of weights in the load_dir is less than the model's weights (when using AC), this causes only the policy network to be loaded.",
  "no_load": "Whether to load weights for the pred_network (False) or not (True).",
  "prev_load_dir": "List of directories to load the previous previous environments' experiences from.",
  "new_load_dir": "Directory to load the short-term system's knowledge from (use only when wm == True).",
  "loss_scale": 'Scaling parameter between DQN loss and pseudo-rehearsal loss (larger = larger DQN loss).',
  "n_reh_ims": 'Number of (pseudo-)items from the previous environments to rehearse with images from the current environment.',
  "wm": 'Whether the long-term system should learn the new task by being taught it from a prelearnt short-term system (True) or from scratch (False).',
  "policy_only": "Whether the long-term system should learn the policy (True) or the policy + value function from the short-term system (only used if wm == True).",
  "ewc_lambda": "Scaling factor for EWC constraint.",
  "ewc_n_samples": "Number of samples used for calculating Fisher information matrix.",
  "online_ewc": 'Whether to use online-EWC (True) or standard EWC (False).',
  "ewc_discount": 'Gamma parameter for online-EWC only.',
  "exp_dir": "Directory to load the current task's experience replay from (used for calculating Fisher overlap).",
  "prev_exp_dir": "Directory to load the previous task's (before env_name) experience replay from (used for calculating Fisher overlap).",
  "prev_prev_exp_dir": "Directory to load the previous task's (before prev_env_name) experience replay from (used for calculating Fisher overlap).",
  "gan_load_dir": 'Directory to load the GAN from.',
  "dual_head": 'Whether the long-term system should have different output units for the policy and value.',
  "dual_scale": 'Scaling parameter between value loss and policy loss (larger = larger value loss) (only used when dual_head == True).',
  "net_type": "Network type to use ['DQN', 'AC'].",
  "policy_B": "Weighting factor for policy loss (only used when net_type == 'AC').",
  "entropy_B_start": "Initial weighting factor for entropy loss (only used when net_type == 'AC').",
  "entropy_B_end": "Final weighting factor for entropy loss (only used when net_type == 'AC').",
  "entropy_B_t_end": "Timestep to linearly decay the entropy weighting factor until (only used when net_type == 'AC').",

  "normQ": "Whether to standard normalise the short-term system's Q-values during teaching (True) or not (False). The mean and std used in this normalisation is approximated by passing 1000 batches of states from the new task through the short-term system's network.",
  "teach_gen_filename": "Directory to load a replay buffer of generated items from (incl. file extension). If this is not None, this buffer of generated items is used to teach the new task to LTM."
}
for key in defaults.keys():
  val = defaults[key]
  descrpt = flags_d[key]
  if type(val) == int:
    flags.DEFINE_integer(key, val, descrpt)
  elif type(val) == bool:
    flags.DEFINE_boolean(key, val, descrpt)
  elif type(val) == str or val is None:
    flags.DEFINE_string(key, val, descrpt)
  elif type(val) == float:
    flags.DEFINE_float(key, val, descrpt)
  elif type(val) == list:
    flags.DEFINE_string(key, str(val), descrpt)
  else:
    print("Error: key's type not recognised (%s: %s)." % (key, type(val)))
    sys.exit()
args = flags.FLAGS
np.random.seed(args.seed)
tf.set_random_seed(args.seed)

def main(_):
  """ Run model in either training, testing or compute Fisher Overlap mode. """
  # check mode, net_type and setup arguments
  assert(args.mode in ['std', 'cf', 'reh', 'pr', 'ewc', 'F_overlap']), "Mode must be either 'std', 'cf', 'reh', 'pr', 'ewc', 'F_overlap'."
  assert(args.net_type in ['DQN', 'AC']), "Network type must be either 'DQN', 'AC'."

  for flag in ['observation_dims', 'filter_nums', 'filter_shapes', 'filter_strides',
               'layer_order', 'hid_nums','prev_env_name', 'prev_load_dir']:
    attr = getattr(args, flag, None)
    if attr is not None:
      setattr(args, flag, ast.literal_eval(attr))

  attr = getattr(args, "experience_length", None)
  setattr(args, "experience_length", int(attr))

  attr = getattr(args, "n_reh_ims", None)
  if attr != None:
    setattr(args, "n_reh_ims", int(attr))

  # setup environments
  env = AtariEnv(args.env_name, args.observation_dims, seed=args.seed)
  env_test = AtariEnv(args.env_name, args.observation_dims, test=True,
    display=args.display, seed=args.seed)

  if args.mode != 'std':
    # TODO: move asserts to the specific dqn classes
    if args.mode == 'reh' or args.mode == 'ewc':
      assert(len(args.prev_env_name) == (len(args.prev_load_dir)+1)), "Number of previous environments must equal the number of dirs in 'prev_load_dir' plus 1!"
    prev_env_test = []
    for i in range(len(args.prev_env_name)):
      prev_env_test.append(AtariEnv(args.prev_env_name[i], args.observation_dims, test=True,
        display=args.display, seed=args.seed))

  # check dual memory system hyper-parameters are set correctly and that the
  # save folder can be created or restored (if specified)
  assert(not(args.wm and args.new_load_dir is None)), "'new_load_dir' must be specified with wm = True."
  assert(not(args.policy_only and not args.wm)), "'wm' must be True if policy_only = True."

  if not args.train or args.restore:
    assert(os.path.isdir(args.save_dir)), "Error[main]: could not find args.save_dir (%s) to restore!" % args.save_dir
  else:
    assert(not os.path.isdir(args.save_dir)), "Error[main]: args.save_dir (%s) already exists!" % args.save_dir
    os.makedirs(args.save_dir)

  sess = tf.Session()

  # initialise agent
  if args.mode == 'std':
    if args.net_type == 'DQN':
      from dqn import DQN
      AgentClass = DQN
    elif args.net_type == 'AC':
      from ac import AC
      AgentClass = AC
    agent = AgentClass(sess, env, env_test, args)
  elif args.mode == 'cf':
    if args.net_type == 'DQN':
      from dqn_cf import DQN_CF
      AgentClass = DQN_CF
    elif args.net_type == 'AC':
      from ac_cf import AC_CF
      AgentClass = AC_CF
    agent = AgentClass(sess, env, env_test, prev_env_test, args)
  elif args.mode == 'reh':
    if args.net_type == 'DQN':
      from dqn_pr import DQN_PR
      AgentClass = DQN_PR
    elif args.net_type == 'AC':
      from ac_pr import AC_PR
      AgentClass = AC_PR
    agent = AgentClass(sess, env, env_test, prev_env_test, False, args)
  elif args.mode == 'pr':
    if args.net_type == 'DQN':
      from dqn_pr import DQN_PR
      AgentClass = DQN_PR
    elif args.net_type == 'AC':
      from ac_pr import AC_PR
      AgentClass = AC_PR
    agent = AgentClass(sess, env, env_test, prev_env_test, True, args)
  elif args.mode == 'ewc':
    from dqn_ewc import DQN_EWC
    agent = DQN_EWC(sess, env, env_test, prev_env_test, args)
  elif args.mode == 'F_overlap':
    from dqn_F_overlap import DQN_F_Overlap
    agent = DQN_F_Overlap(sess, env, env_test, prev_env_test, args)

  # run agent
  if args.mode == 'F_overlap':
    agent.print_fisher_overlaps()
  elif args.train:
    agent.train(args.t_train_max, args.checkpoint_hz, args.test_hz, args.restore)
  else:
    agent.test()



# run on GPU0
if __name__ == '__main__':
  os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
  os.environ["CUDA_VISIBLE_DEVICES"] = "0"
  tf.app.run()
