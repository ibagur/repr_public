import tensorflow as tf
from logger import Logger
import numpy as np
import gzip
import pickle
import sys

class Stats:
  """ Defines a stats recorder, which collects stats about training and keeps a
  record of the best network weights found so far. """
  def __init__(self, sess, save_weights, n_record_update, n_record_names,
    save_dir, log_hz, min_loss=None):
    """ Initialises the stats recorder. This class keeps track of the average reward
    and episode length as well as any other values specified by n_record_names.

      Parameters
        sess: Tensorflow session.
        save_weights: List of weight tensors to keep track of best values found.
        n_record_update: Number of values to keep a record of when the network updates.
        n_record_names: Names of the values to keep a record of when the network
                        updates. This list must include 'av_loss'.
        save_dir: Directory to save and log to.
        log_hz: Frequency to record stats.
        min_loss: Whether the best network should be the network with the minimum
                  average loss received over the current recording period (True)
                  or the maximum average reward over that period (False).
                  Alternatively, if None is specified, the best network is the
                  most recent one.
    """
    self.sess = sess
    self.log = Logger(save_dir, "stats")

    self.best_av_r = -np.inf

    self.min_loss = min_loss
    self.best_av_loss = np.inf

    self.best_t = 0
    self.save_weights = save_weights
    self.best_weights = None

    self.n_record_update = n_record_update
    self.n_record_names = n_record_names
    self.av_loss_index = self.n_record_names.index("av_loss")

    self.save_dir = save_dir

    self.reset()

    self.log_hz = log_hz

    self.av_r_placeholder = tf.placeholder(tf.float32, name="av_r_placeholder")
    self.av_ep_len_placeholder = tf.placeholder(tf.float32, name="av_ep_len_placeholder")
    train_summary = [tf.summary.scalar("av_r", self.av_r_placeholder),
      tf.summary.scalar("av_ep_len", self.av_ep_len_placeholder)]
    self.record_placeholders = []
    for i in range(n_record_update):
      self.record_placeholders.append(tf.placeholder(tf.float32, name=n_record_names[i]+"_placeholder"))
      train_summary.append(tf.summary.scalar(n_record_names[i], self.record_placeholders[i]))
    self.train_summary = tf.summary.merge(train_summary)

  def reset(self):
    """ Resets recording values for the start of a new recording period. """
    self.r_sum = 0
    self.d_sum = 0
    self.update_vals = np.zeros(self.n_record_update)
    self.n_records = 0
    self.update_freq = 0

  def record(self, t, reward, done, update_vals, update, summary_writer):
    """ Record the values specified, logging stats if the current recording period
    has ended.

    Parameters
      t: Current timestep.
      reward: Current reward.
      done: Whether the current episode is terminal.
      update_vals: List of values to record if an update occurred.
      update: Whether an update occurred.
      summary_writer: Tensorflow Summary Writer.
    """
    sys.stdout.flush()
    if self.best_weights == None:
      self.best_weights = self.sess.run(self.save_weights)

    self.r_sum += reward
    self.d_sum += done
    self.n_records += 1

    if update:
      self.update_vals += update_vals
      self.update_freq += 1

    if (t + 1) % self.log_hz == 0:
      print("\nRecording results, do not exit!")
      sys.stdout.flush()
      try:
        av_r = self.r_sum / self.d_sum
      except ZeroDivisionError:
        av_r = np.nan
      try:
        av_ep_len = self.n_records / self.d_sum
      except ZeroDivisionError:
        av_ep_len = np.nan

      av_update_vals = self.update_vals / self.update_freq

      if self.min_loss is None or (not self.min_loss and av_r > self.best_av_r) or \
        (self.min_loss and av_update_vals[self.av_loss_index] < self.best_av_loss):
        self.best_av_r = av_r
        self.best_av_loss = av_update_vals[self.av_loss_index]
        self.best_t = t
        self.set_best_weights()

      s = "t: %d, av_r: %.2f, av_ep_len: %.1f" % (t, av_r, av_ep_len)
      for i in range(self.n_record_update):
        s += ", %s: %.3f" % (self.n_record_names[i], av_update_vals[i])
      s += ", best_av_r: %.2f, best_av_loss: %.3f, best_t: %d" % (self.best_av_r, self.best_av_loss, self.best_t)
      self.log.print(s)

      d = {x:y for x, y in zip(self.record_placeholders, av_update_vals)}
      d[self.av_r_placeholder] = av_r
      d[self.av_ep_len_placeholder] = av_ep_len
      summary = self.sess.run(self.train_summary, feed_dict=d)
      summary_writer.add_summary(summary, t)

      self.reset()
      print("done.")
      sys.stdout.flush()


  def checkpoint(self):
    """ Saves the current stats recorder. """
    pickle_vars = [self.best_weights, self.best_av_r, self.best_t, self.r_sum, self.d_sum, self.update_vals, self.n_records, self.update_freq]
    with gzip.open(self.save_dir+'stats.pckl.gz', 'w') as f:
      pickle.dump(pickle_vars, f, protocol=pickle.HIGHEST_PROTOCOL)

  def restore(self):
    """ Restores a past stats recorder. """
    with gzip.open(self.save_dir+'stats.pckl.gz', 'r') as f:
      self.best_weights, self.best_av_r, self.best_t, self.r_sum, self.d_sum, self.update_vals, self.n_records, self.update_freq = pickle.load(f)

  def set_best_weights(self):
    """ Sets the best weights as the current weight values. """
    self.best_weights = self.sess.run(self.save_weights)

  def load_best_weights(self):
    """ Replaces the network's weights with the best weights recorded. """
    for i in range(len(self.best_weights)):
      self.sess.run(tf.assign(self.save_weights[i], self.best_weights[i]))