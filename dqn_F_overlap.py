from dqn import DQN
from dqn_ewc import DQN_EWC
import sys
import numpy as np
import tensorflow as tf
from experience import Experience

class DQN_F_Overlap(DQN):
  """ Defines a DQN agent from which the Fisher Overlap score can be calculated on. """
  def __init__(self, sess, env, env_test, prev_env_test, args):
    """ Initialises the DQN agent.

    Parameters
      sess: Tensorflow session.
      env: Current environment to learn/play (should be a child class of Env).
      env_test: Current environment to be tested in (should be a child class of Env).
      prev_env_test: List of previously learnt environments to be tested in (should be a child
                     class of Env).
      args: Command line arguments defining the DQN agent.
    """
    assert(len(prev_env_test) <= 2), "'DQN_F_Overlap' does not support task sequences longer than 3."
    assert(args.load_dir is not None), "'load_dir' must be specified!"
    self.prev_env_test = prev_env_test
    assert(args.exp_dir is not None), "'exp_dir' must be specified to calculate the fisher overlap.'"
    self.exp_dir = args.exp_dir
    assert(args.prev_exp_dir is not None), "'prev_exp_dir' must be specified to calculate the fisher overlap.'"
    self.prev_exp_dir = args.prev_exp_dir
    if len(prev_env_test) == 2:
      assert(args.prev_prev_exp_dir is not None), "'prev_prev_exp_dir' must be specified to calculate the fisher overlap.'"
    self.prev_prev_exp_dir = args.prev_prev_exp_dir
    self.ewc_n_samples = args.ewc_n_samples
    assert(not args.dual_head), "'dual_head' cannot be True when calculating Fisher overlap."
    DQN.__init__(self, sess, env, env_test, args)

  def get_test_summary_names(self):
    """ Names of test statistics to be recorded by Tensorflow Summary.

    Returns
      List of names.
    """
    names = []
    for i in range(len(self.prev_env_test)+1):
      names.append("T%d_av_r_test"%(i+1))
      names.append("T%d_std_test"%(i+1))
      names.append("T%d_min_test"%(i+1))
      names.append("T%d_max_test"%(i+1))
    return names

  def calc_fisher_overlap(self, F1, F2):
    """ Calculate the Fisher Overlap between the two matrices.

    Parameters
      F1: One Fisher information matrix.
      F2: Another Fisher information matrix.
    """
    F1 = np.concatenate([f.flatten() for f in F1])
    F1 = F1/np.sum(F1)

    F2 = np.concatenate([f.flatten() for f in F2])
    F2 = F2/np.sum(F2)
    return 1 - np.sum(F1 + F2 - 2*np.sqrt(F1*F2))/2

  def print_fisher_overlaps(self):
    """ Prints the Fisher Overlap scores between all task pairs. """
    self.sess.run(tf.global_variables_initializer())
    import gzip, pickle
    ws = sum(self.pred_network.weights, [])
    with gzip.open(self.load_dir+'stats.pckl.gz', 'r') as f:
      best_weights = pickle.load(f)[0]
    for i in range(len(best_weights)):
      self.sess.run(tf.assign(ws[i], best_weights[i]))

    self.t = -1

    self.record_test_results(False)

    prev_experience = Experience("NCHW", self.batch_size, self.history_length,
      self.experience_length, self.observation_dims)
    prev_experience.restore(self.exp_dir)
    F1 = DQN_EWC.calc_fisher(self.sess, self.pred_network, self.s, prev_experience, self.ewc_n_samples, self.n_actions)
    for f in F1:
      print(f.shape, np.mean(f))
    print()

    prev_experience = Experience("NCHW", self.batch_size, self.history_length,
      self.experience_length, self.observation_dims)
    prev_experience.restore(self.prev_exp_dir)
    F2 = DQN_EWC.calc_fisher(self.sess, self.pred_network, self.s, prev_experience, self.ewc_n_samples, self.n_actions)
    for f in F2:
      print(f.shape, np.mean(f))
    print()

    if len(self.prev_env_test) == 2:
      prev_experience = Experience("NCHW", self.batch_size, self.history_length,
        self.experience_length, self.observation_dims)
      prev_experience.restore(self.prev_prev_exp_dir)
      F3 = DQN_EWC.calc_fisher(self.sess, self.pred_network, self.s, prev_experience, self.ewc_n_samples, self.n_actions)
      for f in F3:
        print(f.shape, np.mean(f))
      print()

    # Note: T1 is most previous task but F1 is most recent
    if len(self.prev_env_test) == 1:
      Ov12 = self.calc_fisher_overlap(F2, F1)
      self.log.print("Overlap(TASK1, TASK2) =", Ov12)
    else:
      Ov12 = self.calc_fisher_overlap(F3, F2)
      self.log.print("Overlap(TASK1, TASK2) =", Ov12)
      Ov13 = self.calc_fisher_overlap(F3, F1)
      self.log.print("Overlap(TASK1, TASK3) =", Ov13)
      Ov23 = self.calc_fisher_overlap(F2, F1)
      self.log.print("Overlap(TASK2, TASK3) =", Ov23)
      av = np.mean([Ov12, Ov13, Ov23])
      self.log.print("Average overlap =", av)

  def train(self, n_steps, checkpoint_hz, test_hz, restore):
    """ This class does not train the agent. """
    raise NotImplementedError

  def test(self):
    """ This class does not test the agent. """
    raise NotImplementedError

  def record_test_results(self, save_sum=True, save_vid=False):
    """ Compute and record the current agent's test results.

    Parameters
      save_sum: Whether to save the results in the Tensorflow Summary (True) or not (False).
      save_vid: Whether to save the results as a video (True) or not (False).
    """
    print("\nRecording testing results, do not exit!")
    sys.stdout.flush()

    all_test_results = []
    for i in range(len(self.prev_env_test)):
      if save_vid:
        self.prev_env_test[i].start_recording(self.save_dir+"T%d.mp4"%(i+1))
      test_results = self.play(self.prev_env_test[i])
      if save_vid:
        self.prev_env_test[i].end_recording()
      self.log.print("[TEST-TASK%d] t: %d,"%(i+1, self.t), "av: %f, std: %f, min: %f, max: %f" % test_results)
      all_test_results.extend(test_results)

    if save_vid:
      self.env_test.start_recording(self.save_dir+"T%d.mp4"%(i+2))
    test_results = self.play(self.env_test)
    if save_vid:
      self.env_test.end_recording()
    self.log.print("[TEST-TASK%d] t: %d,"%(i+2, self.t), "av: %f, std: %f, min: %f, max: %f" % test_results)
    all_test_results.extend(test_results)

    if save_sum:
      summary = self.sess.run(self.test_summary, feed_dict={x:y for x, y in zip(self.test_placeholders, all_test_results)})
      self.summary_writer.add_summary(summary, self.t)

    print("done.")
    sys.stdout.flush()