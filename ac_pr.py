from ac_cf import AC_CF
from dqn_pr import DQN_PR

class AC_PR(DQN_PR):
  """ Defines an AC agent which can use a dual memory system. This agent retains
  previously learnt knowledge through (pseudo-)rehearsal. """
  def __init__(self, sess, env, env_test, prev_env_test, use_pseudo_ims, args):
    """ Initialises the AC agent.

    Parameters
      sess: Tensorflow session.
      env: Current environment to learn/play (should be a child class of Env).
      env_test: Current environment to be tested in (should be a child class of Env).
      prev_env_test: List of previously learnt environments to be tested in (should be a child
                     class of Env).
      use_pseudo_ims: Whether rehearsal should be done with pseudo-items (True) or
                      real items (False).
      args: Command line arguments defining the AC agent.
    """
    self.loss_scale = args.loss_scale
    self.n_reh_ims = args.n_reh_ims
    self.use_pseudo_ims = use_pseudo_ims
    if self.use_pseudo_ims:
      self.gan_load_dir = args.gan_load_dir
      assert(self.gan_load_dir is not None), "'gan_load_dir' must be specified when 'use_pseudo_ims' == True."
    if not self.use_pseudo_ims:
      self.prev_load_dir = args.prev_load_dir
    else:
      self.prev_load_dir = None
    AC_CF.__init__(self, sess, env, env_test, prev_env_test, args)