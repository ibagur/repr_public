from dqn_cf import DQN_CF
import numpy as np
import tensorflow as tf
import gzip
import pickle
import sys

class DQN_PR(DQN_CF):
  """ Defines a DQN agent which can use a dual memory system. This agent retains
  previously learnt knowledge through (pseudo-)rehearsal. """
  def __init__(self, sess, env, env_test, prev_env_test, use_pseudo_ims, args):
    """ Initialises the DQN agent.

    Parameters
      sess: Tensorflow session.
      env: Current environment to learn/play (should be a child class of Env).
      env_test: Current environment to be tested in (should be a child class of Env).
      prev_env_test: List of previously learnt environments to be tested in (should be a child
                     class of Env).
      use_pseudo_ims: Whether rehearsal should be done with pseudo-items (True) or
                      real items (False).
      args: Command line arguments defining the DQN agent.
    """
    self.loss_scale = args.loss_scale
    self.n_reh_ims = args.n_reh_ims
    self.use_pseudo_ims = use_pseudo_ims
    if self.use_pseudo_ims:
      self.gan_load_dir = args.gan_load_dir
      assert(self.gan_load_dir is not None), "'gan_load_dir' must be specified when 'use_pseudo_ims' == True."
    if not self.use_pseudo_ims:
      self.prev_load_dir = args.prev_load_dir
    else:
      self.prev_load_dir = None
    DQN_CF.__init__(self, sess, env, env_test, prev_env_test, args)

  def get_update_record_names(self):
    """ Names of statistics to be recorded by Stats.

    Returns
      List of names.
    """
    if self.dual_head:
      return ['av_q', 'av_policy_loss', 'av_value_loss', 'av_policy_pr_loss', 'av_value_pr_loss', 'av_loss', 'av_net_loss', 'av_pr_loss']
    return ['av_q', 'av_loss', 'av_net_loss', 'av_pr_loss']

  def find_best_net_from_loss(self):
    """ Returns that Stats should record the best network based upon either:
    - Dual memory system: the minimum average loss received over the current
                          recording period.
    - Standard DQN: the most recent network.

    Returns
      Boolean.
    """
    if self.wm:
      return True
    return None

  def build_networks(self):
    """ Builds the networks used by the DQN agent. """
    super().build_networks()
    self.s_copy = tf.placeholder(tf.float32, [None,] + self.in_dims,
      name='s_copy')
    self.s_prev_net = tf.placeholder(tf.float32, [None,] + self.in_dims,
      name='s_prev_net')
    self.pred_network_copy = self.network(self.s_copy, "pred_network", True, reuse=True, dual_head=self.dual_head)
    self.prev_network = self.network(self.s_prev_net, "prev_network", False, dual_head=self.dual_head)

  def build_loss(self):
    """ Builds the loss function used by the DQN agent. """
    with tf.variable_scope('loss'):
      super().build_loss()
      self.dqn_loss = self.loss
      if self.policy_only:
        #self.pseudo_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.pred_network_copy.outputs, labels=tf.one_hot(tf.argmax(self.prev_network.outputs, 1), self.n_actions)))
        self.pseudo_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.pred_network_copy.outputs, labels=tf.nn.softmax(self.prev_network.outputs)))
      else:
        if self.dual_head:
          self.value_pseudo_loss = tf.reduce_mean(tf.square(self.pred_network_copy.value - self.prev_network.value))
          #self.policy_pseudo_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.pred_network_copy.logits, labels=tf.one_hot(tf.argmax(self.prev_network.logits, 1), self.n_actions)))
          self.policy_pseudo_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.pred_network_copy.logits, labels=tf.nn.softmax(self.prev_network.logits)))
          self.pseudo_loss = self.dual_scale*self.value_pseudo_loss + (1-self.dual_scale)*self.policy_pseudo_loss
        else:
          self.pseudo_loss = tf.reduce_mean(tf.square(self.prev_network.outputs - self.pred_network_copy.outputs))
      self.loss = self.loss_scale * self.dqn_loss + (1 - self.loss_scale) * self.pseudo_loss

  def setup_train(self, restore):
    """ Setups the agent to be trained.

    Parameters
      restore: Whether to restore from a previous training session (True) or not (False).
    """
    if not restore:
      get_ims_batch_size = 100
      if self.use_pseudo_ims:
        from gan import GAN
        gan = GAN(self.sess, get_ims_batch_size, self.in_dims, None, self.gan_load_dir, True)

    super().setup_train(restore)

    if not restore:
      # load prev_network weights
      import gzip, pickle
      ws = sum(self.prev_network.weights, [])
      with gzip.open(self.load_dir+'stats.pckl.gz', 'r') as f:
        best_weights = pickle.load(f)[0]
      if len(best_weights) > len(ws):
        assert(self.policy_only), "Error[dqn_pr]: 'policy_only' must be True when the length of prev_network weights is greater than the length of the weights being loaded."
        assert(len(best_weights) == (len(ws) + 2*len(self.hid_nums)+2)), "Error[dqn_pr]: Length of prev_network weights does not equal that of the weights being loaded (minus a second output head)."
        for i in range(len(ws)):
          j = i if i < 2*len(self.filter_nums) else i+2*len(self.hid_nums)+2
          self.sess.run(tf.assign(ws[i], best_weights[j]))
      else:
        for i in range(len(ws)):
          self.sess.run(tf.assign(ws[i], best_weights[i]))

      if self.use_pseudo_ims:
        gan.restore_gen_only()
      else:
        from experience import Experience
        experiences = []
        exp = Experience("NCHW", get_ims_batch_size, self.history_length,
          self.experience_length, self.observation_dims)
        exp.restore(self.load_dir)
        experiences.append(exp)
        for i in range(len(self.prev_load_dir)):
          exp = Experience("NCHW", get_ims_batch_size, self.history_length,
            self.experience_length, self.observation_dims)
          exp.restore(self.prev_load_dir[i])
          experiences.append(exp)

      i = 0
      self.pseudo_ims = np.zeros([self.n_reh_ims,] + self.in_dims, dtype=np.uint8)
      while i < self.n_reh_ims:
        if self.use_pseudo_ims:
          ims = gan.gen()
        else:
          ims = experiences[np.random.randint(len(experiences))].sample()[0]
        self.pseudo_ims[i:i+get_ims_batch_size] = ims
        i += get_ims_batch_size
      self.pseudo_idxs = np.random.permutation(self.n_reh_ims)
      self.pseudo_cur_i = 0

  def restore(self):
    """ Restores a past training session, including the experience replay,
    stats and rehearsed items. """
    super().restore()
    with gzip.open(self.save_dir+'pseudo_vars.pckl.gz', 'r') as f:
      pickle_vars = pickle.load(f)
    self.pseudo_idxs, self.pseudo_cur_i = pickle_vars
    self.pseudo_ims = np.load(self.save_dir+'pseudo_ims.npz')['arr_0']

  def checkpoint(self):
    """ Saves the current training session, including the experience replay,
    stats and rehearsed items. """
    super().checkpoint()
    pickle_vars = (self.pseudo_idxs, self.pseudo_cur_i)
    with gzip.open(self.save_dir+'pseudo_vars.pckl.gz', 'w') as f:
      pickle.dump(pickle_vars, f, protocol=pickle.HIGHEST_PROTOCOL)
    np.savez_compressed(self.save_dir+'pseudo_ims.npz', self.pseudo_ims)

  def update(self):
    """ Updates the predictor network.

    Returns
      List containing the batch's mean Q-value, loss, DQN loss and
      (pseudo-)rehearsal loss.
    """
    state_batch, action_tmp, reward_batch, next_state_batch, done_batch = self.experience.sample()

    if self.pseudo_cur_i + self.batch_size >= len(self.pseudo_ims):
      self.pseudo_idxs = np.random.permutation(len(self.pseudo_ims))
      self.pseudo_cur_i = 0

    batch_idxs = self.pseudo_idxs[self.pseudo_cur_i:self.pseudo_cur_i+self.batch_size]
    pseudo_ims = self.pseudo_ims[batch_idxs]

    self.pseudo_cur_i += self.batch_size

    if self.wm:
      feed_dict = {
        self.s: state_batch,
        self.s_: state_batch,
        self.s_copy: pseudo_ims,
        self.s_prev_net: pseudo_ims
      }
    else:
      feed_dict = {
        self.s: state_batch,
        self.s_: next_state_batch,
        self.terminals: done_batch,
        self.rewards: reward_batch,
        self.actions: action_tmp,
        self.s_copy: pseudo_ims,
        self.s_prev_net: pseudo_ims
      }

    if self.dual_head:
      q_t, policy_loss, value_loss, policy_pr_loss, value_pr_loss, loss, dqn_loss, pr_loss, _ = self.sess.run([self.pred_network.value, self.policy_loss, self.value_loss, self.policy_pseudo_loss, self.value_pseudo_loss, self.loss, self.dqn_loss, self.pseudo_loss, self.optim], feed_dict=feed_dict)
      return np.mean(q_t), policy_loss, value_loss, policy_pr_loss, value_pr_loss, loss, dqn_loss, pr_loss
    q_t, loss, dqn_loss, pr_loss, _ = self.sess.run([self.pred_network.outputs, self.loss, self.dqn_loss, self.pseudo_loss, self.optim], feed_dict=feed_dict)
    return np.mean(q_t), loss, dqn_loss, pr_loss