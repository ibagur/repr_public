class Env:
  """ Defines the main functions of an environment. This class contains methods that a
  child class must override. """
  def __init__(self, n_actions, test=False):
    """ Initialises the environment.

    Parameters
      n_actions: Number of valid actions in the environment.
      test: Whether this environment is for testing (True) or training (False).
    """
    self.n_actions = n_actions
    self.test = test

  def step(self, a):
    """ Make an action in the environment, returning the result.

    *************************************
    * This method should be overridden. *
    *************************************

    Parameter
      a: Action.

    Returns
      List containing the observed state, reward and terminal.
    """
    raise NotImplementedError

  def reset(self):
    """ Reset the environment.

    *************************************
    * This method should be overridden. *
    *************************************

    Returns
      Observed state.
    """
    raise NotImplementedError